import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { IonicStorageModule } from '@ionic/storage';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { StarRatingModule } from 'ionic3-star-rating';
import { HTTP } from '@ionic-native/http';
// import { Http, Headers, RequestOptions } from '@angular/common/http';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { EvaCheck1Page } from '../pages/eva-check1/eva-check1';
import { EvaDetailsPage } from '../pages/eva-details/eva-details';
import { EvaRatingPage } from '../pages/eva-rating/eva-rating';
import { EvaResultPage } from '../pages/eva-result/eva-result';
import { OrgAreaPage } from '../pages/org-area/org-area';
import { LoginPage } from '../pages/login/login';
import { RegisterPage } from '../pages/register/register';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    EvaCheck1Page,
    EvaDetailsPage,
    EvaRatingPage,
    EvaResultPage,
    OrgAreaPage,
    LoginPage,
    RegisterPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    StarRatingModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    EvaCheck1Page,
    EvaDetailsPage,
    EvaRatingPage,
    EvaResultPage,
    OrgAreaPage,
    LoginPage,
    RegisterPage
  ],
  providers: [
    StatusBar,
    HTTP,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
