import { Component } from '@angular/core';
import { NavController, NavParams,  MenuController, AlertController, LoadingController, } from 'ionic-angular';
import { RegisterPage } from '../register/register';
import { HomePage } from '../home/home';
import { Storage } from '@ionic/storage';
// import { Http } from '@angular/http';
// import 'rxjs/add/operator/map';
// import 'rxjs/Rx';
// import 'rxjs/add/operator/map'
// import { map } from 'rxjs/operators';

import { HTTP } from '@ionic-native/http';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
	posts: any;
	datas = {};
	status : any; url:any;
  constructor(public navCtrl: NavController,public navParams: NavParams, private menu: MenuController, public http: HTTP, public alertCtrl: AlertController, public loadingCtrl: LoadingController, public storage: Storage) {
  	this.storage.remove('userData');
  }


 ionViewDidEnter() {
    this.menu.swipeEnable(false);
    //  	this.navCtrl.setRoot(HomePage);
  }

  doLogin()
  {

console.log("do Login -------");

// this.http.get('https://syafiqevo.000webhostapp.com/5s/login.php?userName=aaaaaaaa&userPass=aaaaaaaa', {}, {})
//   .then(data => {
//   	var data1 = JSON.parse(data.data);

//     console.log("data:", data.data);
//     console.log("data[0]-----:", data1[0].userId); // data received by server
//   })
//   .catch(error => {

//     console.log(error.status);
//     console.log(error.error); // error message as string
//     console.log(error.headers);

//   });

  	var username = this.datas['username'];
  	var password = this.datas['password'];

 	if (username != undefined && password != undefined)
 	{

		const loading = this.loadingCtrl.create({
		    content: 'Loggin in...'
		  });

  		loading.present();
this.http.get('https://syafiqevo.000webhostapp.com/5s/login.php?userName='+username+'&userPass='+password, {}, {})
  .then(data => {
  	  	var data1 = JSON.parse(data.data);

 			var stat = data1[0].status;

 			console.log("from apiii: ", data1);
      console.log(data);
			if (stat == "pass")
			   	{
            this.storage.set('userData', data1[0]);  					
            loading.dismiss();					
			   		console.log('pass');
			   		this.navCtrl.push(HomePage);
			  		this.navCtrl.setRoot(HomePage);
			   	}
			else
			   	{
			   		this.showAlertIncorrect();
			   		loading.dismiss();	
			   		console.log('x pass');
		   		}
		  })
		  .catch(error => {

		    console.log(error.status);
		    console.log(error.error); // error message as string
		    console.log(error.headers);

		  });
 	}

 	else
 	{
 		 this.showAlertIncorrect();
 		console.log('x masuk kosong je');
 	}
  }

  doRegister()
  {
  	this.navCtrl.push(RegisterPage);
  }

  showAlertIncorrect() 
  {
    let alert = this.alertCtrl.create({
      title: 'Error',
      subTitle: 'Incorrect Username or Password',
      buttons: ['OK']
    });
    alert.present();
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

}
