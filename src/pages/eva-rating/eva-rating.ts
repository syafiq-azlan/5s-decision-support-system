import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { HTTP } from '@ionic-native/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the EvaRatingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-eva-rating',
  templateUrl: 'eva-rating.html',
})
export class EvaRatingPage {
datas:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public http: HTTP, public loadingCtrl: LoadingController, private storage: Storage) {



      storage.get('userData').then((id) => {

            const loading = this.loadingCtrl.create({
        content: 'Loading...'
      });

      loading.present();

            var userId = id.userId;
            console.log("user id eva rating: ", userId);
this.http.get('https://syafiqevo.000webhostapp.com/5s/getOrg.php?userId='+userId, {}, {})
  .then(data => {
        var data1 = JSON.parse(data.data);

    this.datas = data1;
                    loading.dismiss();          
    });
  	            });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EvaRatingPage');
  }


}
