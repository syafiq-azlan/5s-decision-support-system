import { Component } from '@angular/core';
import { NavController, ModalController } from 'ionic-angular';
import { EvaDetailsPage } from '../eva-details/eva-details';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController, public modalCtrl: ModalController) {

  }
 goEvaDetails() {
    this.navCtrl.push(EvaDetailsPage);
  }

  presentPrompt() {

   let profileModal = this.modalCtrl.create(EvaDetailsPage, { userId: 8675309 });
   profileModal.present();
}
}
