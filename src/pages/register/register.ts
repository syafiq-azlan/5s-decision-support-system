import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';

import { HTTP } from '@ionic-native/http';
import 'rxjs/add/operator/map';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

data = {};
	status : any;

  strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])"
                          +"(?=.*[!@#\$%\^&\*])(?=.{8,})");

  constructor(public navCtrl: NavController, public navParams: NavParams,  public http: HTTP, public alertCtrl: AlertController, public loadingCtrl: LoadingController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
  }

  doRegister()
  {
      var username = this.data['username'];
      var password = this.data['password'];
      var email = this.data['email'];
      var phone = this.data['phone'];
  if (username != undefined && password != undefined && email != undefined && phone != undefined)
  {
     if(this.data['password'].length > 7)
        {

        console.log("ok");

            const loading = this.loadingCtrl.create({
        content: 'Loggin in...'
      });

      loading.present();

this.http.get('https://syafiqevo.000webhostapp.com/5s/register.php?userName='+username+'&userPass='+password+'&userEmail='+email+'&userPhone='+phone, {}, {})
  .then(data => {
  	  	var data1 = JSON.parse(data.data);
  	  	var status = data1[0].status;
        console.log("srtat register: ", status);
                    loading.dismiss();          
           if (status == "pass")
           {
                  let alert = this.alertCtrl.create({
                title: 'Success',
                subTitle: 'Your account have been created',
                       buttons: [
                        {
                          text: 'Ok',
                          role: 'Ok',
                          handler: () => {
                            console.log('ok clicked');
                            this.navCtrl.pop();
                        }
                      }]
              });
              alert.present();
           }
           else
           {
                  let alert = this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Please check your details, the username might already taken',
                buttons: ['Ok']
              });
              alert.present();
           }
        });  
    
        }
        else
        {
          this.showPasswordError("pswd less than 8");
        }
  }

  else
  {
     this.showAlertIncorrect();
    console.log('x masuk kosong je');
  }

  }

  showPasswordError(from)
  {
    let alert = this.alertCtrl.create({
        title: 'Password Error',
        subTitle: 'Your passwords must be at least 8 characters long',
        buttons: ['Ok']
      });
      alert.present();
      console.log("error from:", from);
    }
  

  showAlertIncorrect() 
  {
    let alert = this.alertCtrl.create({
      title: 'Error',
      subTitle: 'Please try again',
      buttons: ['Ok']
    });
    alert.present();
  }

  showAlertCorrect() 
  {
    let alert = this.alertCtrl.create({
      title: 'Success',
      subTitle: 'Your accound have been created',
      buttons: 
      [
        {
          text: 'Ok',
          handler: () => {
             this.navCtrl.pop();
            console.log('ok clicked');
          }
        },
      ]
    });
    alert.present();
  }

}
