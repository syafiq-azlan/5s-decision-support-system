import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { OrgAreaPage } from '../org-area/org-area';
import { Storage } from '@ionic/storage';
import { HTTP } from '@ionic-native/http';
import 'rxjs/add/operator/map';
/**
 * Generated class for the EvaCheck1Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-eva-check1',
  templateUrl: 'eva-check1.html',
})
export class EvaCheck1Page {
datas = {};
items = []; kosong = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, private storage: Storage,  public http: HTTP) {
  	
	this.datas['a1'] = 1;this.datas['a2'] = 1;this.datas['a3'] = 1;this.datas['a4'] = 1;this.datas['a5'] = 1;this.datas['a6'] = "";
	this.datas['b1'] = 1;this.datas['b2'] = 1;this.datas['b3'] = 1;this.datas['b4'] = 1;this.datas['b5'] = 1;this.datas['b6'] = "";
	this.datas['c1'] = 1;this.datas['c2'] = 1;this.datas['c3'] = 1;this.datas['c4'] = 1;this.datas['c5'] = 1;this.datas['c6'] = "";
	this.datas['d1'] = 1;this.datas['d2'] = 1;this.datas['d3'] = 1;this.datas['d4'] = 1;this.datas['d5'] = 1;this.datas['d6'] = "";
	this.datas['e1'] = 1;this.datas['e2'] = 1;this.datas['e3'] = 1;this.datas['e4'] = 1;this.datas['e5'] = 1;this.datas['e6'] = "";


  }

doCalculate()
{
	var a1 = this.datas['a1'] + this.datas['a2'];


	console.log("test a1 tambah 1a2 = "+a1);

}

doRmv()
{
	//this.storage.clear();
	//this.storage.remove('areaData');
   this.navCtrl.pop();
 // this.storage.get('areaData').then((val) => {
 //    console.log('Your data arr1 is', val);
 //  });

}
doDone()
{

    const loading = this.loadingCtrl.create({
        content: 'Loading...'
      });

      loading.present();

	var totalAreamark = this.datas['a1']+this.datas['a2']+this.datas['a3']+this.datas['a4']+this.datas['a5']+this.datas['b1']+this.datas['b2']+this.datas['b3']+this.datas['b4']+this.datas['b5']+this.datas['c1']+this.datas['c2']+this.datas['c3']+this.datas['c4']+this.datas['c5']+this.datas['d1']+this.datas['d2']+this.datas['d3']+this.datas['d4']+this.datas['d5']+this.datas['e1']+this.datas['e2']+this.datas['e3']+this.datas['e4']+this.datas['e5'];
	var star;
	if (totalAreamark >=1 && totalAreamark <=25)
		star = 1
	else if (totalAreamark >=26 && totalAreamark <=50 )
		star = 2
	else if (totalAreamark >=51 && totalAreamark <=75 )
		star = 3
	else if (totalAreamark >=76 && totalAreamark <=100 )
		star = 4
	else if (totalAreamark >=101 && totalAreamark <=125 )
		star = 5

	var areaId =  this.navParams.get('areaId');
  	console.log("area id from eva check 1,", areaId);

	console.log("total area markkkkkkk = "+totalAreamark +" bintang : = "+ star);

	var appendComment;
	appendComment = this.datas['a6']+' '+this.datas['b6']+' '+this.datas['c6']+' '+this.datas['d6']+' '+this.datas['e6'];
	console.log("append comment === ", appendComment);

	this.http.get('https://syafiqevo.000webhostapp.com/5s/setScore.php?areaId='+areaId+'&totalAreamark='+totalAreamark+'&star='+star+'&appendComment='+appendComment, {}, {})
  .then(data => {
        var data1 = JSON.parse(data.data);

//    this.posts = data;
  
});
             loading.dismiss();  

   this.navCtrl.pop();
		// this.navCtrl.push(OrgAreaPage);

	// let arr1: (string|number)[] = [];



	//messages: number[]
	//var areaDataSection = {name:"John"};
	//let areaDataSection2 = { "name":"11", "age":22, "car":11 };


  // set a key/value
	  // if (stat == 0) //first time
	  // {
			// arr.push({marks: marks, name: name});
			//  this.storage.set('areaData', arr);  	
			// console.log("First time", arr);
	  // }
	  // else //next
	  // {
	  // 	//	arr.push({id: 1, text: 'try2'});
	  // 		//	  		arr.push({id: 1, text: 'try3'});

			// console.log("2nd time", arr);
			//  this.storage.get('areaData').then((val) => {
			//     console.log('not first time is', val);
			//     arr.push(val);
			//     arr.push({marks: marks, name:name});

			//     this.storage.set('areaData', arr);  	
			//   });
	  // }
 // this.kosong.push(JSON.parse(JSON.stringify(this.storage.get('areaData'))));
  // to get a key/value pair


   
 // this.storage.get('areaData').then((val) => {
 //    console.log('Your data after inseirt 2 is', val);
 //  });

	//this.navCtrl.push(OrgAreaPage);
}
  ionViewDidLoad() {
    console.log('ionViewDidLoad EvaCheck1Page');
  }

}
