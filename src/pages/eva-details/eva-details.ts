import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { OrgAreaPage } from '../org-area/org-area';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the EvaDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-eva-details',
  templateUrl: 'eva-details.html',
})
export class EvaDetailsPage {

current: any;
	datas = {};

  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage,  public alertCtrl: AlertController) {
    this.storage.remove('org');
    this.storage.remove('role');
  	var d = new Date(); // for now
	 this.current = d.getDate() + "-" + (d.getMonth()+1) + "-" + d.getFullYear();


  }

  doOrganization()
  {

  		var org = this.datas['org'];
  		var role = this.datas['role'];

      if (org != undefined && role != undefined)
      {
            this.storage.set('org', org);
            this.storage.set('role', role);

            console.log(org +role);
            // this.navCtrl.push(OrgAreaPage);
        this.navCtrl.pop();

          this.navCtrl.push(OrgAreaPage, {
        org: org,
        role: role
        });
      }
      else
      {
    let alert = this.alertCtrl.create({
      title: 'Error',
      subTitle: 'Please fill in the field',
      buttons: ['OK']
    });
    alert.present();
      }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EvaDetailsPage');
  }

  dismiss()
  {
  	this.navCtrl.pop();
  }

  doClear()
  {
     this.navCtrl.pop();   
  }

}
