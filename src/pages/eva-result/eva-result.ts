import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { HTTP } from '@ionic-native/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the EvaResultPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-eva-result',
  templateUrl: 'eva-result.html',
})
export class EvaResultPage {
	display:any;
	starOut:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,  public alertCtrl: AlertController, public loadingCtrl: LoadingController,public http: HTTP, private storage: Storage) {

    const loading = this.loadingCtrl.create({
        content: 'Loading...'
      });

      loading.present();

  	  storage.get('org').then((val) => {
      storage.get('userData').then((id) => {
            var userId = id.userId;

this.http.get('https://syafiqevo.000webhostapp.com/5s/getDetails.php?userId='+userId+'&orgName='+val, {}, {})
  .then(data => {
        var data1 = JSON.parse(data.data);

    this.display = data1;
   // var score = data['areaName'];
         console.log("org nameeeee eva result : ", data1[0].areaName);

         let sum = 0;
    for (var i = 0; i < data1.length; i++) {
        sum+= parseInt(data1[i].score);
    }

    var finalStar;

    finalStar = (sum/(125*parseInt(data1.length))*5);
    console.log("Final starrrrr:", finalStar);

 	if (finalStar>= 0.0 && finalStar <=1.0)
   		this.starOut = 1;
   	else if (finalStar>= 1.1 && finalStar <=2.0)
   		this.starOut = 2;
   	else if (finalStar>= 2.1 && finalStar <=3.0)
   		this.starOut = 3;
   	else if (finalStar>= 3.1 && finalStar <=4.0)
   		this.starOut = 4;
   	else if (finalStar>= 4.1 && finalStar <=5.0)
   		this.starOut = 5;

    console.log("Final marks " + finalStar + "star out " + this.starOut);

    this.http.get('https://syafiqevo.000webhostapp.com/5s/setOrg.php?userId='+userId+'&orgName='+val+'&orgScore='+this.starOut, {}, {})
  .then(data => {

});
  
 });

           });
  });
            loading.dismiss();          
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EvaResultPage');
  }

  doBack()
  {
  	this.navCtrl.pop();
	
  }
doHome()
{
  	this.navCtrl.pop();
		// this.navCtrl.setRoot(HomePage.);
}

doEmails()
{
	// overAllRating,
    const loading = this.loadingCtrl.create({
        content: 'Loading...'
      });

      loading.present();
	  	  this.storage.get('org').then((orgName) => {

	  	  	     this. storage.get('userData').then((id) => {
            var userId = id.userId;
            var receiver = id.userEmail;

this.http.get('http://syafiqevo.000webhostapp.com/5s/sendMail.php?receiver='+receiver+'&userId='+userId+'&orgName='+orgName+'&overAllRating='+this.starOut, {}, {})
  .then(data => {
        var data1 = JSON.parse(data.data);

    console.log("dataaaaaaa email", data1);
          loading.dismiss();          
    let alert = this.alertCtrl.create({
      title: 'Success',
      subTitle: 'The data have been emailed to your address',
      buttons: ['OK']
    });
    alert.present();
      });
	             });  });

}
}
