import { Component, ElementRef, ViewChild   } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { EvaCheck1Page } from '../eva-check1/eva-check1';
import { EvaResultPage } from '../eva-result/eva-result';
import { HTTP } from '@ionic-native/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the OrgAreaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-org-area',
  templateUrl: 'org-area.html',
})

export class OrgAreaPage {

    todo: string;
    uId: any; dd: any;	datas = {};
    org:any; role:any;
    posts:any;
    area:any;

  constructor(public navCtrl: NavController,  public alertCtrl: AlertController, public navParams: NavParams, public http: HTTP, private storage: Storage, public loadingCtrl: LoadingController) {

    const loading = this.loadingCtrl.create({
        content: 'Loading...'
      });

      loading.present();

  storage.get('org').then((val) => {

      storage.get('userData').then((id) => {
            var userId = id.userId;
            console.log("user iddddddddddddddddddddddddd", userId);

this.http.get('https://syafiqevo.000webhostapp.com/5s/getDetails.php?userId='+userId+'&orgName='+val, {}, {})
  .then(data => {
        var data1 = JSON.parse(data.data);

    this.area = data1;
         console.log("org nameeeee : ", val);
});

});
  });

             loading.dismiss();  

  }

goBack()
{
  this.navCtrl.pop();
}
 add() {

    var org = this.navParams.get('org');
    var role = this.navParams.get('role');
    var areaName =  this.datas['areaName'];

    if (areaName != undefined)
    {
    const loading = this.loadingCtrl.create({
        content: 'Loading...'
      });

      loading.present();

  this.storage.get('org').then((val) => {
            
                  this.storage.get('userData').then((id) => {
            var userId = id.userId;

    console.log("from org areaaaa: " + val + role + areaName + userId);

this.http.get('https://syafiqevo.000webhostapp.com/5s/createArea.php?userId='+userId+'&areaName='+areaName+'&userRole='+role+'&orgName='+val+'&status=empty', {}, {})
  .then(data => {
        var data1 = JSON.parse(data.data);

    this.posts = data1;
   this.navCtrl.setRoot(this.navCtrl.getActive().component);
  
});

});


   });           
             loading.dismiss();  
    }
    else
    {
          let alert = this.alertCtrl.create({
      title: 'Error',
      subTitle: 'Please fill in the field',
      buttons: ['OK']
    });
    alert.present();
    }

        // this.todos.push(this.todo);
        // //this.todos.push(this.dd);
        // this.todo = "";
    }
 
   delete(item) {
        // var index = this.todos.indexOf(item, 0);
        // if (index > -1) {
        //     this.todos.splice(index, 1);
        // }
    }

 goStart(areaId)
 {
   console.log("asasasas" + areaId);

 	this.navCtrl.push(EvaCheck1Page, {
	areaId: areaId
	});
 }

 goResult()
 {
     this.navCtrl.pop();
 	this.navCtrl.push(EvaResultPage);
 }
  ionViewDidLoad() {
    console.log('ionViewDidLoad OrgAreaPage');
  }

}
